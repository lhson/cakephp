	<?php
	App::uses('AppController', 'Controller');
	/**
	 * Posts Controller
	*/
	class UsersController extends AppController {

	/**
	 * Components
	*/
	
	public $components = array('Paginator','Session','Filebinder.Ring');
	public $helper = array('Html', 'Form');
	//public $name = 'User';
	/**
	* before filter only view
	*/

	// public function beforeFilter() {
	// 	parent::beforeFilter();
	// 	$this->Auth->allow('index');
	// }

	/**
	 * index method
	*/
	public function index() {
		  // $order = 'User.name ASC';
		$fields = array('User.*','countries.*','cities.*');

		$joins = array();
		$conditions = array();
		$joins[] = array(
			'table' => 'countries',
			'foreignKey' => false,
			'conditions' => 'countries.id = User.country_id',
			'type' => 'LEFT',
			'alias' => 'countries',
		);
		$joins[] = array(
			'table' => 'regions',
			'foreignKey' => false,
			'conditions' => 'cities.id = User.city_id',
			'type' => 'LEFT',
			'alias' => 'cities',
		);
		$conditions[] = array(
			'User.status' => 1,
		);
		//$limit = array('limit' => 3);
		//$log = $this->User->getDataSource()->getLog(false, false);
		//debug($log);exit;
		$this->paginate = array(
			'limit' => 10,
			'joins' => $joins,
			'fields' => $fields,
			'conditions'=> $conditions
		);
		$users = $this->paginate('User');
		// $log = $this->User->getDataSource()->getLog(false, false);
		// debug($log);exit;
		$this->set(compact('users'));
	}

	/**
	 * view method
	 *
	 */
	public function view($id = null) { 
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}

		$joins =  array (
			array(
				'table' => 'regions', 
				'alias' => 'Region', 
				'type' => 'LEFT',
				'conditions' => array(
					'User.city_id = Region.id',

				)
			),
			array(
				'table' => 'countries', 
				'alias' => 'Country', 
				'type' => 'LEFT',
				'conditions' => array(
					'User.country_id = Country.id '
				)
			)
		);


		$userDetail = $this->User->find('first',
			array(
				"fields" => array('User.*','Region.*','Country.*'),
				'joins' => $joins,
				'conditions'=>array('User.id' => $id )));



		$this->set('user',$userDetail);
	}
	/**
	 * edit method
	 */
	public function edit($id = null) {
			// var_dump(Configure::read('file_path'));
			// exit;
		$this->User->id = $id;
		$this->loadModel('Country');
		$this->loadModel('Region');
		$countries = $this->Country->find('list');
		if ($this->request->is(array('post', 'put'))) {
			 $this->Ring->bindUp('User');
			$user = $this->request->data;
			$this->request->data['User']['id'] = $id;
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been saved.');
				return $this->redirect(array('action' => 'index'));
			} else {

				$cities = array();
				if(!empty($user['User']['country_id']))
				{
					$cities = $this->Region->find('list', array(
						'conditions' => array('Region.country_id' => $user['User']['country_id']	,
							'not' =>  array('Region.name' => '' )
						),
						'fields'=>(array('Region.id','Region.name'))
					));
				}	
				$this->Session->setFlash('The post could not be saved. Please, try again.');
				
			}
		} 
		else {
			$options = array('conditions' => array('User.id' => $id));
			$user = $this->User->find('first', $options);				
			$cities = array();
			if(!empty($user['User']['country_id']))
			{
				$cities = $this->Region->find('list', array(
					'conditions' => array('Region.country_id' => $user['User']['country_id']	,
						'not' =>  array('Region.name' => '' )
					),
					'fields'=>(array('Region.id','Region.name'))
				));
			}	
			$this->request->data = $user;

			
		}$this->set('cities', $cities);
		$this->set('countries', $countries);
	}

	/**	
	 * delete method
	 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		//get user current want delete => uodate status = -1
		$curruser =  $this->User->find('first',
			array('conditions' => array('User.id' => $id

		)));
		$this->request->onlyAllow('post', 'delete');
		$this->User->status = 2;
		 //var_dump($curruser);
		// exit;
		if($this->User->save($this->User)){
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	* function search with ajax
	*/
	public function search($keyword ='') {
		$this->autoRender = false;
		$this->layout = 'ajax';

		if ($this->request->is('ajax')) {		
	  		// join condition 
			
			$joins = array();
			$joins[] = array(
				'table' => 'countries',
				'foreignKey' => false,
				'conditions' => 'countries.id = User.country_id',
				'type' => 'LEFT',
				'alias' => 'countries',
			);
			$joins[] = array(
				'table' => 'regions',
				'foreignKey' => false,
				'conditions' => 'cities.id = User.city_id',
				'type' => 'LEFT',
				'alias' => 'cities',
			);

			//$limit = array('limit' => 2);
			$fields = array('User.*','countries.*','cities.*');

			if(!empty($this->request->data))
			{
				$filterstatus = (isset($this->request->data['search']['status']))?$this->request->data['search']['status']:1;
				$keyword =$this->request->data['search']['keyword'];
	    	//if you user search multi value with comma
	    	//check string exist will flow if get multi condition
				$arrkey = explode(' ', $keyword);
				$or_condition = array();
				foreach ($arrkey as $value){
					// $or_condition['OR'][] = array_merge($or_condition, 

					// 		array('OR' => array('User.name LIKE' => '%'.$value.'%')), 
					// 		array('OR' => array('countries.name LIKE' => '%'.$value.'%')),
					// 		array('OR' => array('cities.name LIKE' => '%'.$value.'%'))
					// 	);
					$or_condition['OR'][] = array('OR' => array('User.name LIKE' => '%'.$value.'%'));
					$or_condition['OR'][] = array('OR' => array('countries.name LIKE' => '%'.$value.'%'));
					$or_condition['OR'][] = array('OR' => array('cities.name LIKE' => '%'.$value.'%'));
				}
					// print_r($or_condition);
					// exit;
				$conditions = array( 'OR' =>$or_condition);
				$conditions[] = array(
					'User.status' => $filterstatus,
				);

				$this->paginate = array(
					'limit' => 10,
					'joins' => $joins,
					'fields' => $fields,
					'conditions'=> $conditions
				);
				$users = $this->paginate('User');
			//var_dump(Constants::$config);
				$this->set(compact('users'));
				//debug
			 // $log = $this->User->getDataSource()->getLog(false, false);
				// debug($log);exit;
		//  pass variable to view
		//call view render html with view name ajax
				$view = new View($this, false);
				$content = $view->element('view_ajax');
				// debug($this->paginate);
				// exit;
		// $content = $this->render('view_ajax','ajax');
		//add array view render to array response
				$response = array();
				// $response['conditions'] = $conditions;
				$response['status']='success';
				$response['message'] = 'do you like it';
				$response['data'] =  $content;
				echo json_encode($response);


			} else {
				$response['status']='error';
				$response['message'] = 'wrong data';
				$response['data'] =  '';
				echo json_encode($response);
			}
			exit;
		}
	}
}






