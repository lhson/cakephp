<?php
App::uses('AppController', 'Controller');
App::uses('JsBaseEngineHelper', 'View/Helper');
/**
 * Posts Controller
*/
class CitysController extends AppController {


		public function getcityByCountries() {
			if($this->request->is('ajax')) {
			$this->autoRender = false;
			$country_id = $this->request->data['country_id'];
			$cities = $this->City->find('list', array(
        		'conditions' => array('City.country_id' => $country_id),'limit'=>10,
        		'fields'=>(array('City.id','City.name'))
   			 ));
			echo json_encode($cities);
			

			$this->layout = 'ajax';

		}
 	// 	$this->autoRender = false;
		// // $citys= $this->City->find('list', array(
		// // 	'conditions' => array('City.country_id' => $country_id),
		// // 	'recursive' => -1
		// // 	));
   	 //$cities = $this->City->find('all', array('conditions'=>array('City .country_id'=>$country_id)));
	    
	}

}

    