<?php
App::uses('AppController', 'Controller');
App::uses('JsBaseEngineHelper', 'View/Helper');
/**
 * Posts Controller
*/
class RegionsController extends AppController {


		public function getregionsByCountries() {
			if($this->request->is('ajax')) {
				$this->autoRender = false;
				$country_id = $this->request->data['country_id'];
				$cities = $this->Region->find('list', array(
	        		'conditions' => array('Region.country_id' => $country_id,
	        		'not' =>  array('Region.name' => '' )
	        		),
	        		'fields'=>(array('Region.id','Region.name'))
	   			 ));
				echo json_encode($cities);
				$this->layout = 'ajax';

		}
 	
	    
	}

}

    