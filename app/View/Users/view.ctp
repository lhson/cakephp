<style>


.hidden {
   visibility: hidden;
}
 .image img {
  width: 300px;
  height: auto;
}

</style>
<div class="User view">
	<h2><?php echo __('User info detail'); ?></h2>
	<dl>
		<dt> avatar </dt>
		<dd class="image"><?php echo $this->Label->image($user['User']['avatar']);?></dd>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('gender'); ?></dd>
		<dd> <?php echo h(Constants::$config['gender'][$user['User']['gender']]);  ?></dd>
		<dt> birthday </dt>
		<dd><?php echo __('Birthday'); ?></dd	>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($user['Country']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Provine'); ?></dt>
		<dd>
			<?php echo h($user['Region']['name']); ?>
			&nbsp;
		</dd>

	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li>     <?php
                    if($user['User']['status'] != 2) {
                        echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); 
                    }
                    ?> 
         </li>
		<li><?php echo $this->Html->link(__('List User'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
