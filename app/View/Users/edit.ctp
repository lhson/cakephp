<?php echo $this->Html->script('jquery-migrate-1.4.1.min'); // Include jQuery library?>
<div class="users form">
  <?php echo $this->Form->create('User',array('type'=>'file')); ?>
  <fieldset>
    <legend><?php echo __('users'); ?></legend>
    <?php

    echo $this->Form->input('name',array('required'=> false));
    echo $this->Form->input('email',array('type'=> 'text', 'required'=> false));
    $options =  Constants::$config['gender'];
    $status =  Constants::$config['status'];
    $attributes = array(
      'legend' => false,
      'required' => false,
      'hiddenField' =>false 
    );
    


echo $this->Form->label('gender', '<span class="label2">Gender</span><span class=" required2">*</span>');
echo $this->Form->input('gender', array(
        'div' => true,
        'label' => true,
        'type' => 'radio',
        'legend' => false,
        'required' => false,
        'options' => $options
    )
);
echo $this->Form->input('status', array(
        'div' => true,
        'label' => true,
        'type' => 'radio',
        'legend' => false,
        'required' => false,
        'options' => $status
    )
);
    echo $this->Form->input('birthday', array( 'label' => 'Date of birth', 
      'dateFormat' => 'DMY', 
      'minYear' => date('Y') - 70,
      'maxYear' => date('Y') - 18,
      'required' => false,
      'empty' => array(
        'day' => '-- Day --', 'month' => '-- Month --', 'year' => '-- Year --',

      )
    ));
    echo $this->Form->input('country_id', 
      array(
       'empty' => 'Select',
       'options' =>  $countries,
       'class' => 'span5',
       'required' => false,
       'error' => array('attributes' => array('wrap' => 'span', 'class' => 'label custom-inline-error label-important help-inline')),
     ));
    echo $this->Form->input('city_id', 
      array( 
        'options' => $cities,
        'empty' => 'Select',
        'class' => 'span5',
        'required' => false,
        'error' => array('attributes' => array('wrap' => 'span', 'class' => 'label custom-inline-error label-important help-inline'))
      ));
      ?>
     <?php   echo $this->Form->input('avatar',array(
      'type'=> 'file',
      'required'=> false)); ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
  </div>	
  <div class="actions">
   <h3><?php echo __('Actions'); ?></h3>
   <ul>

    <li><?php echo $this->Html->link(__('List users'), array('action' => 'index')); ?></li>
  </ul>
</div>


<script> 
  $(document).ready(function(){
   //$('#UserCityId').attr('disabled', 'disabled');
   $('#UserCountryId').on("change",function() {

     var id = ($(this).val());
   		//call ajax send id city and receive list cities of contry id
   		//append inside city value

<?php
      $path =FULL_BASE_URL.Router::url(['controller' => 'regions', 'action' => 'getregionsByCountries']);
 ?>
 var path =  '<?php echo $path; ?>';

 console.log(path);
   		$.ajax({
        type: 'POST',
        url:  path,
        data: {country_id:id},
        cache: false,
        dataType: 'HTML',
        beforeSend: function(){
          $('#na').html('Checking...');
        },
        success: function (data){
         
          $('#UserCityId').find('option:not(:first)').remove();

         //$('#UserCityId').removeAttr('disabled');;
         $.each(JSON.parse(data), function(key,value) {

          $('#UserCityId')
          .append($('<option></option>')
           .attr('value', key)
           .text(value));

        })
         console.log(JSON.parse(data));


       }
     });
     });

 });
</script>