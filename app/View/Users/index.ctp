<style>


.hidden {
   visibility: hidden;
}
 .image img {
  width: 50px;
  height: 50px;
}

</style>

<?php echo $this->Html->script('jquery-migrate-1.4.1.min'); ?> 
<div class="posts index">
    <h2><?php echo __('List Users'); ?></h2>
    <?php echo $this->Form->create('search',array('default' => false)); ?>
    <p><span class="message_err hidden" style="color: red">please find search correct</span></p>
    <?php
    echo $this->Form->input('keyword', array(
        'label' => false,
        'placeholder' => 'Find something you want !!'
    )); 
    $status =  Constants::$config['status'];
    ?>
    <p style = "color: green;font-weight: 500">fillter user by </p>

    <?php 
    echo $this->Form->input('status', array(
        'div' => true,
        'label' => true,
        'type' => 'radio',
        'legend' => false,
        'required' => false,
        'hiddenField'=>false,
        'options' => $status,
        'value' => 1
    )   
); ?>
    <?php echo $this->Form->end('search');?>
    <div class="data_render">
        <?php echo $this->element('view_ajax'); ?>
    </div>
        <div class="actions">
            <h3><?php echo __('Actions'); ?></h3>
            <ul>
                <li><?php echo $this->Html->link(__('New User'), array('action' => 'edit')); ?></li>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){

    /**
    * 1. hander event click of button search
    * 2. get keywork => validate  common error key word
    * empty, special text
    * send ajax to action search
    * 3. receive array json of users search found out
    * 4. apeend inside table current user and clean current data
    */
    <?php
    $path =FULL_BASE_URL.Router::url(['controller' => 'users', 'action' => 'search']);
    ?>
    var path =  '<?php echo $path; ?>';
    var nameReg = /^[a-z\.\s-]{2,}$/;
    $('#searchKeyword').on("keypress", function(e) {
        if (e.keyCode == 13) {
             var senddata = $("#searchIndexForm").serializeArray();
                //call ajax
                $.ajax({
                    type: 'POST',
                    url:  path,
                    data: senddata,
                    cache: false,
                    dataType: 'HTML',
                    beforeSend: function(){
                      $('.data_render').html('<span style="color: green; font-size: 20px text-aligin: center">loading ...</span>');
                  },
                  success: function (data){
                   successaction(data);
               }
           });

            }
        });
    //funtion ajax handel event click button
     $('#searchIndexForm').on("submit", function() {
         //1. get value of key search 
          //2 get value of fillter 
         // var keyword = $('#searchKeyword').val();
          var senddata = $(this).serializeArray();
        
             $.ajax({
                    type: 'POST',
                    url:  path,
                    data: senddata,
                    cache: false,
                    dataType: 'HTML',
                    beforeSend: function(){
                      $('.data_render').html('<span style="color: green; font-size: 20px text-aligin: center">loading ...</span>');
                  },
                  success: function (data){
                   successaction(data);
               }
           });
        });
        //function success
        function successaction(data)
        {
            var respone = $.parseJSON(data);           
            $('.message_err').removeClass('hidden');
            $('.message_err').html('find success').css('color','green');
            $('.data_render').html(respone['data']);
        }

    });    





























</script>