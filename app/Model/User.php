<?php
App::uses('AppModel', 'Model','CakeTime', 'Utility');
/**
 * Post Model
 *
 */
class User extends AppModel {
	//validate json
  /*
    1 .validate username
      not empty,lengh <= 20, user in anphbet, user is unique
    2 email:
      not empty, length <= 20, validate email, email is unique
    3 gender: must true sex value in 0 or 1
    4: country must choose,
    5: birhtday
     notempty, format date correct, is real day follow in calendar 
  */
    //  public $actsAs = array(
    //         'Filebinder.Bindable' => array(
    //                 'dbStorage' => false
    //         )
    // );

    public $name = 'User';
    public $actsAs = array('Filebinder.Bindable');
      
    public $bindFields = array();
   /**
    * checkIllegalCode
    * check include illegal code
    *
    * @param $filePath
    * @return
    */
  
     var $validate = array(
      'name' => array(
       'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'Name is required'
      ) ,
       'maxlength' => array(
        'rule' => array('maxLength', 20),
        'message' => 'maxlength length of 20 characters'
      ),
       'aphabet' => array(
        'rule' => '/^[a-zA-Z\s]*$/',
        'message' => 'user name only letter and space'
      )
     ),
      'email' => array(
        'required' => array(
          'rule' => array('email'),
          'message' => 'your email not for verification.'
        ),
        'maxLength' => array(
          'rule' => array('maxLength', 50),
          'message' => 'Email cannot be more than 50 characters.'
        ),
        'unique' => array(
          'rule' => 'uniqueActive',
          'on' => 'update',
          'message' => 'Your Email already exists.'
        ),
        'uniquecreate' => array(
          'rule' => 'isUnique',
          'on' => 'create',
          'message' => 'Your Email already exists.'
        )
      ),
      'gender' => array(
       'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'gender is required'
      ) ,
       'invalue' => array(
         'rule' =>  array('inList', array(0,1)),
         'message' => 'sex only male or female' 
       )
     ),
      'birthday' => array(
     // 'validatereal' => array(
     // 'rule' => array('date_checker','-'), 
     // 'message' => 'Please choose correct day' 
     // ),
       'valid' => array(
        'rule' => 'date',
        'message' => 'Enter a valid date',
        
      )
     ),
      'country_id' => array(
        'rule' => 'notEmpty',
        'message' => 'Select your country'
      ),
      'city_id' => array(
        'rule' => 'notEmpty',
        'message' => 'Select your city'
      ),
    );



     public function uniqueActive($data)
     {
      $options = array(
        'conditions' => array(
          'User.id !=' => $this->data['User']['id'], 'User.email' => $this->data['User']['email']
        )
      );

      if($this->find('count', $options)){
        return false;
      }
      return true;
    }
    function __construct()
    {
        parent::__construct();
        $this->bindFields[] = array(
                'field' => 'avatar',
                 'tmpPath' => Configure::read('file_path').'tmp/',
                'filePath' => Configure::read('file_path').'upload/',
        );
    }



  }